<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Song;
use wapmorgan\Mp3Info\Mp3Info;
use getID3;

class SongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allsongs = Song::get()->all();

        return response()->json($allsongs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // // $media = MediaFile::open(`http://127.0.0.1:8000/uploads/`.$request->song_name);

        // // if ($media->isAudio()) {
        // //     $audio = $media->getAudio();
        // //     $audioDuration = `.floor($audio->duration)`;
        // //     $audioTagSongs = `.$audio->tags1['artist']`;
        // //   }

        // $audio = new Mp3Info(`../../public/audio/`.$request->song_name, true);

        // $audioDuration = ($audio->tags1);
        // // $audioTagSongs = 'Songs'.':'.$audio->tags1['artist'];

        // // $getId = new getID3;
        // // $duration = $getId->analyze(`../../public/audio/`.$request->song_name);

        // return response()->json($audioDuration);

        $getID3 = new getID3;
        $ThisFileInfo = $getID3->analyze(`../../public/audio/`.$request->song_name);
        $len= @$ThisFileInfo['playtime_string'];

        $newsong = Song::create([
            'title' => $request->song_title,
            'length' => $len,
            'artist' => $request->song_artist,
        ]);

        return response()->json($newsong);
    }

    public function uploadSong(Request $request) {
        $songName = time().'.'.$request->file->extension();
        // $location = public_path('audio/' . $songName);
        $music = $request->file->move(public_path('audio'), $songName);
        return $music;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Song::destroy($id);
        return response()->json($delete);
    }
}

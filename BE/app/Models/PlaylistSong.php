<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlaylistSong extends Model
{
    use HasFactory;
    public $fillable = ['song_id', 'playlist_id'];
    
    public function song(){
        return $this->belongsTo(Song::class, 'song_id', 'id');
    }

    public function playlist(){
        return $this->belongsTo(Playlist::class, 'playlist_id', 'id');
    }
}

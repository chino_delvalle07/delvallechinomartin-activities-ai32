import Vue from 'vue';
import VueRouter from 'vue-router';

import Index from '../views/Index.vue';
import MP3Player from '../views/Mp3Player.vue';
import ViewPlaylist from '../views/Playlist.vue';

Vue.use(VueRouter);

const routes = [{
        path: '/',
        name: 'index',
        component: Index,
        children: [{
                path: '/',
                name: 'MP3Player',
                components: {
                    MP3Player: MP3Player,
                },
            },
            {
                path: '/playlistsongs/:slug',
                name: 'viewplaylist',
                components: {
                    viewplaylist: ViewPlaylist,
                },
            },
        ],
    },
    // {
    //     path: '/playlist/:slug',
    //     name: 'viewplaylist',
    //     component: {
    //         viewplaylist: ViewPlaylist,
    //     },
    // },
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
});

export default router;
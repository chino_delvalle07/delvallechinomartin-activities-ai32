import API from '../../base/';

export default {
    namespaced: true,
    state: {
        songs: {},
        playlist_songs: [],
    },
    getters: {},
    mutations: {
        SET_SONGS(state, data) {
            state.songs = data;
        },
        SET_PLAYLIST_SONGS(state, data) {
            state.playlist_songs = data;
        },
    },
    actions: {
        async saveSong({ commit }, payload) {
            const res = await API.post('songs', payload)
                .then((res) => {
                    return res;
                })
                .catch((err) => {
                    return err.response;
                });

            return res;
        },
        async addToPlaylist({ commit }, data) {
            const res = await API.post(`addtoplaylist/${data.id}`, data)
                .then((res) => {
                    return res;
                })
                .catch((err) => {
                    return err.response;
                });

            return res;
        },
        async getPlaylistSongs({ commit }, { data }) {
            const res = await API.get(`playlistsongs/${data.id}`)
                .then((res) => {
                    commit('SET_PLAYLIST_SONGS', res.data);
                    return res;
                })
                .catch((err) => {
                    return err.response;
                });

            return res;
        },
        async getSongs({ commit }, data) {
            const res = await API.get('songs', data)
                .then((res) => {
                    commit('SET_SONGS', res.data);
                    return res;
                })
                .catch((err) => {
                    return err.response;
                });

            return res;
        },

        async deleteSong({ commit }, id) {
            const res = await API.delete(`songs/${id}`)
                .then((res) => {
                    return res;
                })
                .catch((err) => {
                    return err.response;
                });

            return res;
        },

        /** END OF ACTION */
    },
};
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import VueFileAgent from 'vue-file-agent';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';

import './assets/css/style.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'bootstrap-icons/font/bootstrap-icons.css';
import 'vue-file-agent/dist/vue-file-agent.css';

Vue.use(BootstrapVue);
Vue.use(VueFileAgent);
Vue.use(IconsPlugin);

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount('#app');